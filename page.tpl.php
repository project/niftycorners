<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/niftyCorners.css";</style>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/niftycube.js"></script>
<script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/niftylayout.js"></script>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
</head>
<body <?php print theme("onload_attribute"); ?>>
<div id="header">

<div id="logo">
<?php if ($logo) : ?>
<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" border="0" alt="<?php print t('Home') ?>" /></a>
<?php endif;?>

<?php if ($site_name) : ?>
<h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print($site_name) ?></a></h1><?php endif;?>
</div>

<?php if (isset($primary_links)) : ?>
<div id="menu">
<?php if ($site_slogan) { ?><div id="slogan"><?php print $site_slogan ?></div><?php } ?>
<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
</div>
<?php endif; ?>




        <?php if (isset($primary_links)) : ?>
          
        <?php endif; ?>






</div>

<div id="container">

<?php if ($sidebar_left) { ?>
<div id="sidebar-left">
<?php print $sidebar_left ?>
</div>
<?php } ?>


<?php if ($sidebar_right) : ?>
<div id="content_normal">
<?php else: ?>
<div id="content_wide">
<?php endif; ?>


<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>

<div class="odd">
<?php print $breadcrumb ?>
</div>

<div class="comments">
      <?php print $help ?>
      <?php print $messages ?>
</div>

<?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
      
      <div class="tabs"><?php print $tabs ?></div>

      <?php print $content; ?>

</div>

<?php if ($sidebar_right != ""): ?>
<div id="sidebar-right">
        <?php print $sidebar_right ?> <!-- print right sidebar if any blocks enabled -->
</div>
<?php endif; ?> 

<div id="footer">
<?php print $footer_message ?>
</div>
<?php print $closure ?>
</div>
</body>
</html>