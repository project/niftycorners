<div class="node<?php if ($sticky) { print " sticky"; } ?>">
<?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?><div class="nodecontent"><?php if ($picture) { print $picture; }?>
<span class="taxonomy"><?php print $terms?></span>
<div class="submitted"><?php print $submitted?></div>
<div class="content"><?php print $content?></div>
<?php if ($links) { ?><div class="links">&raquo; <?php print $links?></div><?php }; ?>
</div>
</div>
