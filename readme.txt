/* start: for Drupal 5.x */

This theme is a port of Nifty Corners Layout by Alessandro Fulciniti (http://webdesign.html.it/articoli/leggi/528/more-nifty-corners/). 

This theme is fixed width for 1024x768 resolution (960px for header image). It supports 3 columns layout and can be 2 columns if you disable the right sidebar. It supports logo/slogan/mission and looks ok in IE/Firefox.

If you are using Drupal in a subdirectory, maybe you need modify the css path in niftycube.js:
l.setAttribute("href","/themes/niftyCorners/niftyCorners.css");

/* end: for Drupal 5.x */